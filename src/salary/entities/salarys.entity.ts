import { CheckIn } from 'src/checkIn/entities/checkIn.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    default: 0,
  })
  totalPrice: number;

  @Column({
    default: 0,
  })
  totalHour: number;

  @Column({
    default: 'card',
  })
  paymentMethod: string;

  @Column({
    default: 0,
  })
  managerId: number;

  @Column({ type: 'date' })
  payDate: Date; //YYYY-MM-DD HH:MM:SS

  @Column({
    default: false,
  })
  status: boolean;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(() => User, (user) => user.salarys)
  user: User;

  @OneToMany(() => CheckIn, (CheckIn) => CheckIn.salary)
  checkin: CheckIn[];
}
