import { Module } from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Salary } from './entities/salarys.entity';
import { SalarysController } from './salarys.controller';
import { User } from 'src/users/entities/user.entity';
import { CheckIn } from 'src/checkIn/entities/checkIn.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Salary, User, CheckIn])],
  controllers: [SalarysController],
  providers: [SalarysService],
  exports: [SalarysService],
})
export class SalarysModule {}
