import { User } from 'src/users/entities/user.entity';

export class CreateSalaryDto {
  payDate: Date;
  paymentMethod: string;
  status: boolean;
  managerId: number;
  user: User;
}
