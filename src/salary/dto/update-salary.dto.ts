import { PartialType } from '@nestjs/mapped-types';
import { CreateSalaryDto } from './create-Salary.dto';

export class UpdateSalaryDto extends PartialType(CreateSalaryDto) {}
