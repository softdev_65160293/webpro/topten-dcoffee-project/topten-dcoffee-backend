import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  //UseGuards,
} from '@nestjs/common';
import { SalarysService } from './salarys.service';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
//import { AuthGuard } from 'src/auth/auth.guard';

//@UseGuards(AuthGuard)
@Controller('salarys')
export class SalarysController {
  constructor(private readonly salarysService: SalarysService) {}

  @Post()
  create(@Body() createSalaryDto: CreateSalaryDto) {
    return this.salarysService.create(createSalaryDto);
  }

  @Get()
  findAll() {
    console.log('find all');
    return this.salarysService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.salarysService.findOne(+id);
  }

  @Get('users/:id')
  findAllByUserId(@Param('id') id: string) {
    return this.salarysService.findAllByUserId(+id);
  }

  @Get('branch/:BranchId')
  findAllByUserBranch(@Param('BranchId') BranchId: string) {
    return this.salarysService.findAllByUserBranch(BranchId);
  }

  @Post(':id')
  update(@Param('id') id: string, @Body() updateSalaryDto: UpdateSalaryDto) {
    return this.salarysService.update(+id, updateSalaryDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.salarysService.remove(+id);
  }
}
