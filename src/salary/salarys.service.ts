import { Injectable } from '@nestjs/common';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salarys.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { CheckIn } from 'src/checkIn/entities/checkIn.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(CheckIn)
    private checkinsRepository: Repository<CheckIn>,
  ) {}
  async create(createSalaryDto: CreateSalaryDto) {
    const salary = new Salary();
    salary.paymentMethod = createSalaryDto.paymentMethod;
    salary.payDate = new Date(createSalaryDto.payDate);
    salary.managerId = createSalaryDto.managerId;

    const allCheckIn = await this.checkinsRepository.find({
      where: {
        userId: createSalaryDto.user.id,
        status: false,
      },
    });

    let allHour = 0;
    for (const checkIn of allCheckIn) {
      allHour += checkIn.hour;
    }

    salary.totalHour = allHour;

    const nowUser = await this.usersRepository.findOne({
      where: { id: createSalaryDto.user.id },
    });

    salary.totalPrice = allHour * nowUser.workRate;

    salary.user = nowUser;

    return this.salarysRepository.save(salary);
  }

  findOne(salaryId: number) {
    return this.salarysRepository.find({
      where: { id: salaryId },
      relations: ['user'],
    });
  }

  findAll() {
    return this.salarysRepository.find({
      relations: ['user'],
    });
  }

  findAllByUserId(userId: number) {
    return this.salarysRepository.find({
      where: { user: { id: userId } },
      relations: ['user'],
    });
  }

  async findAllByUserBranch(branchId: string) {
    const result = await this.salarysRepository.query(`
      SELECT * 
      FROM salary 
      LEFT JOIN user ON salary.userId = user.id
      WHERE user.branchId = ${branchId};`);
    return result;
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salarysRepository.findOneOrFail({
      where: { id },
    });
    console.log(updateSalaryDto);

    salary.status = true;
    this.salarysRepository.save(salary);
    console.log('Update Salary');
    return salary;
  }

  async remove(id: number) {
    const deleteProduct = await this.salarysRepository.findOneOrFail({
      where: { id },
    });
    await this.salarysRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
