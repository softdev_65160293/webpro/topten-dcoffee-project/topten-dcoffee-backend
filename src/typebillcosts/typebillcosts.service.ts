import { Injectable } from '@nestjs/common';
import { CreateTypebillcostDto } from './dto/create-typebillcost.dto';
import { UpdateTypebillcostDto } from './dto/update-typebillcost.dto';
import { Typebillcost } from './entities/typebillcost.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TypebillcostsService {
  constructor(
    @InjectRepository(Typebillcost)
    private typebillcostsRepository: Repository<Typebillcost>,
  ) {}
  create(createTypebillcostDto: CreateTypebillcostDto) {
    const typebillcost = new Typebillcost();
    typebillcost.name = createTypebillcostDto.name;
    return this.typebillcostsRepository.save(typebillcost);
  }

  findAll() {
    return this.typebillcostsRepository.find();
  }

  findOne(id: number) {
    return this.typebillcostsRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateTypebillcostDto: UpdateTypebillcostDto) {
    const typebillcost = await this.typebillcostsRepository.findOneOrFail({
      where: { id },
    });
    typebillcost.name = updateTypebillcostDto.name;
    this.typebillcostsRepository.save(typebillcost);
    return typebillcost;
  }

  async remove(id: number) {
    const deleteTypebillcost = await this.typebillcostsRepository.findOneOrFail(
      {
        where: { id },
      },
    );
    await this.typebillcostsRepository.remove(deleteTypebillcost);

    return deleteTypebillcost;
  }
}
