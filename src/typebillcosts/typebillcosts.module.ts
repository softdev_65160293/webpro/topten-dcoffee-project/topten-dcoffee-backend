import { Module } from '@nestjs/common';
import { TypebillcostsService } from './typebillcosts.service';
import { TypebillcostsController } from './typebillcosts.controller';
import { Typebillcost } from './entities/typebillcost.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Typebillcost])],
  controllers: [TypebillcostsController],
  providers: [TypebillcostsService],
})
export class TypebillcostsModule {}
