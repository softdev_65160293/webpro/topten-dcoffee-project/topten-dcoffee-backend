import { Module } from '@nestjs/common';
import { StocksService } from './stocks.service';
import { StocksController } from './stocks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from './entities/stock.entity';
import { Branch } from 'src/branchs/entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Stock, Branch])],
  controllers: [StocksController],
  providers: [StocksService],
})
export class StocksModule {}
