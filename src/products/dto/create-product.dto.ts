export class CreateProductDto {
  name: string;

  image: string;

  price: string;

  type: string;

  gsize: string;

  sweet_level: string;

  category: string;
}
