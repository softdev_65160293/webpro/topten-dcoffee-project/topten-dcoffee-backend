import { PartialType } from '@nestjs/mapped-types';
import { CreateReceiptstockDto } from './create-receiptstock.dto';

export class UpdateReceiptstockDto extends PartialType(CreateReceiptstockDto) {}
