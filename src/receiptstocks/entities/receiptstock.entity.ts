import { User } from 'src/users/entities/user.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import { ReceiptStockItem } from './receiptstockItem.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class ReceiptStock {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.receiptStock)
  user: User;

  @Column()
  Date: Date;

  @Column()
  total: number;

  @Column()
  idreceipt: string;

  @Column()
  nameplace: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => ReceiptStockItem,
    (receiptStockItem) => receiptStockItem.receiptStock,
  )
  receiptStockItems: ReceiptStockItem[];

  @ManyToOne(() => Branch, (branch) => branch.receiptStock)
  branch: Branch;
}
