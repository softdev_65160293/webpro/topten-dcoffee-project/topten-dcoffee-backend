import { Injectable } from '@nestjs/common';
import { CreateReceiptstockDto } from './dto/create-receiptstock.dto';
import { UpdateReceiptstockDto } from './dto/update-receiptstock.dto';
import { ReceiptStock } from './entities/receiptstock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ReceiptStockItem } from './entities/receiptstockItem.entity';
import { User } from 'src/users/entities/user.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import { Branch } from 'src/branchs/entities/branch.entity';

@Injectable()
export class ReceiptstocksService {
  @InjectRepository(ReceiptStock)
  private receiptStocksRepository: Repository<ReceiptStock>;
  @InjectRepository(ReceiptStockItem)
  private receiptStockItemsRepository: Repository<ReceiptStockItem>;
  @InjectRepository(User) private usersRepository: Repository<User>;
  @InjectRepository(Stock) private stocksRepository: Repository<Stock>;
  @InjectRepository(Branch) private branchsRepository: Repository<Branch>;

  async create(createReceiptstockDto: CreateReceiptstockDto) {
    console.log('Create RCI Function');

    const receiptStock = new ReceiptStock();
    const stock = new Stock();
    const user = await this.usersRepository.findOneBy({
      id: createReceiptstockDto.userId,
    });
    const branch = await this.branchsRepository.findOneBy({
      id: createReceiptstockDto.branchId,
    });
    receiptStock.user = user;
    receiptStock.branch = branch;
    receiptStock.total = createReceiptstockDto.total;
    receiptStock.idreceipt = createReceiptstockDto.idreceipt;
    receiptStock.nameplace = createReceiptstockDto.nameplace;
    receiptStock.Date = new Date(createReceiptstockDto.date);
    console.log(receiptStock.Date);
    receiptStock.receiptStockItems = [];
    console.log(receiptStock.user);

    for (const ri of createReceiptstockDto.receiptStockItems) {
      const receiptStockitem = new ReceiptStockItem();
      receiptStockitem.stock = await this.stocksRepository.findOneBy({
        id: ri.stockId,
      });
      receiptStockitem.name = receiptStockitem.stock.name;
      receiptStockitem.minimum = receiptStockitem.stock.minimum;
      receiptStockitem.balance = receiptStockitem.stock.balance;
      receiptStockitem.price = receiptStockitem.stock.price;
      receiptStockitem.unit = receiptStockitem.stock.unit;
      receiptStockitem.totalprice = ri.totalprice;
      receiptStockitem.unitPerItem = ri.unitPerItem;
      console.log(ri);
      console.log('UPI ' + ri.unitPerItem);
      await this.receiptStockItemsRepository.save(receiptStockitem);
      receiptStock.receiptStockItems.push(receiptStockitem);
      stock.name = receiptStockitem.name;
      stock.minimum = receiptStockitem.minimum;
      stock.price = receiptStockitem.price;
      stock.unit = receiptStockitem.unit;
      stock.balance = receiptStockitem.balance;
      stock.balance = stock.balance + ri.unitPerItem;
      stock.branch = receiptStockitem.stock.branch;
      console.log('Stockid ' + ri.stockId);
      console.log(stock);
      await this.stocksRepository.update(ri.stockId, stock);
      console.log('Finish Create');
    }
    return this.receiptStocksRepository.save(receiptStock);
  }

  findAll() {
    return this.receiptStocksRepository.find({
      relations: {
        receiptStockItems: true,
      },
      order: {
        id: 'DESC',
      },
    });
  }
  async findAllByBranch(branchId: number) {
    console.log('Branch');
    const branch = JSON.stringify(
      await this.branchsRepository.findOneBy({
        id: branchId,
        stocks: true,
        user: true,
      }),
    );
    console.log(branch);

    const receiptstock = await this.receiptStocksRepository.find({
      where: { branch: JSON.parse(branch) },
      relations: { user: true, receiptStockItems: true },
      order: {
        id: 'DESC',
      },
    });

    // console.log(stocks);

    return receiptstock;
  }

  findOne(id: number) {
    return this.receiptStocksRepository.findOneOrFail({
      where: { id },
      relations: { receiptStockItems: true, user: true },
    });
  }
  async reportDays(days: string) {
    console.log('REPORT FUNCTION');

    console.log('Days ' + days);
    const result = await this.receiptStocksRepository.query(
      `SELECT A.idreceipt, A.Date, B.name AS item_name,A.total, ROUND(A.total/B.unitPerItem,2) AS total_perUnit
      FROM receipt_stock A
      INNER JOIN receipt_stock_item B ON A.id = B.receiptStockId
      WHERE DATE(A.Date) = '${days}'
      ORDER BY A.id;`,
    );
    console.log(result);
    return result;
  }
  async reportYears(years: string) {
    console.log('REPORT Year FUNCTION');
    console.log('Years ' + years);
    const result = await this.receiptStocksRepository.query(
      `SELECT MONTHNAME(calendar.month_year) AS month,
      YEAR(calendar.month_year) AS year,
      COALESCE(SUM(A.total), 0) AS total_month
FROM
 (SELECT '${years}-01-01' AS month_year
  UNION SELECT '${years}-02-01'
  UNION SELECT '${years}-03-01'
  UNION SELECT '${years}-04-01'
  UNION SELECT '${years}-05-01'
  UNION SELECT '${years}-06-01'
  UNION SELECT '${years}-07-01'
  UNION SELECT '${years}-08-01'
  UNION SELECT '${years}-09-01'
  UNION SELECT '${years}-10-01'
  UNION SELECT '${years}-11-01'
  UNION SELECT '${years}-12-01') AS calendar
LEFT JOIN receipt_stock A ON MONTH(A.Date) = MONTH(calendar.month_year)
 AND YEAR(A.Date) = YEAR(calendar.month_year)
GROUP BY YEAR(calendar.month_year), MONTH(calendar.month_year)
ORDER BY YEAR(calendar.month_year), MONTH(calendar.month_year);`,
    );
    console.log(result);
    return result;
  }
  async reportMonth(month: string) {
    console.log('REPORT MONTH FUNCTION');

    console.log('Month ' + month);

    const result = await this.receiptStocksRepository.query(
      `SELECT 
      DATE_FORMAT(dates.date_series, '%Y-%m-%d') AS Date,
      COALESCE(SUM(A.total), 0) AS total
  FROM (
      SELECT DATE_ADD('${month}-01', INTERVAL (t4*1000 + t3*100 + t2*10 + t1) DAY) AS date_series
      FROM
          (SELECT 0 AS t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t1,
          (SELECT 0 AS t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t2,
          (SELECT 0 AS t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) AS t3,
          (SELECT 0 AS t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3) AS t4
      WHERE 
          DATE_ADD('${month}-01', INTERVAL (t4*1000 + t3*100 + t2*10 + t1) DAY) BETWEEN '${month}-01' AND LAST_DAY('${month}-01')
  ) AS dates
  LEFT JOIN receipt_stock A ON DATE_FORMAT(A.Date, '%Y-%m-%d') = DATE_FORMAT(dates.date_series, '%Y-%m-%d')
  GROUP BY DATE_FORMAT(dates.date_series, '%Y-%m-%d')
  ORDER BY DATE_FORMAT(dates.date_series, '%Y-%m-%d');
  `,
    );
    console.log(result);
    return result;
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  update(id: number, updateReceiptstockDto: UpdateReceiptstockDto) {
    return `This action updates a #${id} receiptstock`;
  }

  async remove(id: number) {
    const deleteReceipt = await this.receiptStocksRepository.findOneOrFail({
      where: { id },
    });
    await this.receiptStocksRepository.remove(deleteReceipt);

    return deleteReceipt;
  }
}
