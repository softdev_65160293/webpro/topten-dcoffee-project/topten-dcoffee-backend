import { Test, TestingModule } from '@nestjs/testing';
import { ReceiptstocksController } from './receiptstocks.controller';
import { ReceiptstocksService } from './receiptstocks.service';

describe('ReceiptstocksController', () => {
  let controller: ReceiptstocksController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReceiptstocksController],
      providers: [ReceiptstocksService],
    }).compile();

    controller = module.get<ReceiptstocksController>(ReceiptstocksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
