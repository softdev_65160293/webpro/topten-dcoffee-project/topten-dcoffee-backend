import { Test, TestingModule } from '@nestjs/testing';
import { CheckInsController } from './checkIns.controller';
import { CheckInsService } from './checkIns.service';

describe('CheckInsController', () => {
  let controller: CheckInsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckInsController],
      providers: [CheckInsService],
    }).compile();

    controller = module.get<CheckInsController>(CheckInsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
