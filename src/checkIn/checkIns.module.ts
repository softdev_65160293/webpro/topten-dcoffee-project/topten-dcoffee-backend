import { Module } from '@nestjs/common';
import { CheckInsService } from './checkIns.service';
import { CheckInsController } from './checkIns.controller';
import { CheckIn } from './entities/checkIn.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Salary } from 'src/salary/entities/salarys.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckIn, User, Salary])],
  controllers: [CheckInsController],
  providers: [CheckInsService],
})
export class CheckInsModule {}
