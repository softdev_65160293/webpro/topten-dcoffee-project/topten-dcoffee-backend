import { Salary } from 'src/salary/entities/salarys.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  FindOperator,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckIn {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.checkIns)
  user: User;

  @ManyToOne(() => Salary, (salary) => salary.checkin)
  salary?: Salary;

  @Column({ default: 1 })
  userId: number;

  @Column({ type: 'date' }) // หรือสามารถใช้ { type: 'timestamp' } ได้ตามที่คุณต้องการ
  date: string | Date | FindOperator<Date>;

  @Column({ type: 'time' })
  checkIn: string;

  @Column({ type: 'time' })
  checkOut: string;

  @Column({ default: false })
  status: boolean;

  @Column({ default: 0 })
  hour: number;
  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
