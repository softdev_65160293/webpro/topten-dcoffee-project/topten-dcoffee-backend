import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
  //UseGuards,
} from '@nestjs/common';
import { CheckInsService } from './checkIns.service';
import { CreateCheckInDto } from './dto/create-checkIn.dto';
import { UpdateCheckInDto } from './dto/update-checkIn.dto';
//import { AuthGuard } from 'src/auth/auth.guard';

//@UseGuards(AuthGuard)
@Controller('checkIns')
export class CheckInsController {
  constructor(private readonly checkInsService: CheckInsService) {}

  @Post()
  create(@Body() createCheckInDto: CreateCheckInDto) {
    return this.checkInsService.create(createCheckInDto);
  }

  @Get()
  findAll() {
    console.log('find all');
    return this.checkInsService.findAll();
  }
  @Get('sumHour')
  sumHour() {
    return this.checkInsService.sumHour();
  }

  @Get('getCheckIn')
  findAllSp() {
    console.log('find all');
    return this.checkInsService.findAllSp();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkInsService.findOne(+id);
  }

  @Get('user/:id')
  findOneByUser(@Param('id') id: string) {
    return this.checkInsService.findOneByUser(+id);
  }
  @Get('month/user/:id/:month')
  findAllByMonth(@Param('id') id: string, @Param('month') month: string) {
    return this.checkInsService.findAllInMonthByUserId(+id, +month);
  }
  @Get('users/:id')
  findAllByUserId(@Param('id') id: string) {
    return this.checkInsService.findAllByUserId(+id);
  }

  @Get('branch/:id')
  findAllByBranchId(@Param('id') id: string) {
    return this.checkInsService.findAllByBranchId(+id);
  }
  @Get('year/:year')
  reportYear(@Param('year') year: string) {
    return this.checkInsService.reportYear(year);
  }
  @Get('year/:year/:id')
  reportYearByBranch(@Param('year') year: string, @Param('id') id: string) {
    return this.checkInsService.reportYearByBranch(year, +id);
  }
  @Get('day/:day')
  reportDay(@Param('day') day: string) {
    return this.checkInsService.reportDay(day);
  }
  @Get('day/:day/:id')
  reportByBranch(@Param('day') day: string, @Param('id') id: string) {
    return this.checkInsService.reportYearByBranch(day, +id);
  }
  @Get('month/:month')
  reportMonth(@Param('month') month: string) {
    return this.checkInsService.reportMonth(month);
  }
  @Get('month/:month/:id')
  reportMonthByBranch(@Param('month') month: string, @Param('id') id: string) {
    return this.checkInsService.reportMonthByBranch(month, +id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCheckInDto: UpdateCheckInDto) {
    return this.checkInsService.update(+id, updateCheckInDto);
  }
  @Post('checkOut/:id')
  checkOut(
    @Param('id') id: string,
    @Body() updateCheckInDto: UpdateCheckInDto,
  ) {
    return this.checkInsService.checkOut(+id, updateCheckInDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkInsService.remove(+id);
  }
}
