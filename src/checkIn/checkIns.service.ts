import { Injectable } from '@nestjs/common';
import { CreateCheckInDto } from './dto/create-checkIn.dto';
import { UpdateCheckInDto } from './dto/update-checkIn.dto';
import { CheckIn } from './entities/checkIn.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Like, Repository } from 'typeorm';

@Injectable()
export class CheckInsService {
  @InjectRepository(CheckIn)
  private checkInRepository: Repository<CheckIn>;
  @InjectRepository(User) private usersRepository: Repository<User>;
  async create(createCheckInDto: CreateCheckInDto) {
    console.log('in create');
    console.log(createCheckInDto);
    const checkIn = new CheckIn();
    checkIn.checkIn = createCheckInDto.checkIn;
    checkIn.checkOut = '00:00:00';
    const user = await this.usersRepository.findOneBy({
      id: createCheckInDto.user.id,
    });
    checkIn.user = user;
    checkIn.userId = user.id;
    checkIn.date = new Date(createCheckInDto.date);
    console.log(checkIn);
    return this.checkInRepository.save(checkIn);
  }
  getSecondsFromTimeString(timeString: string): number {
    console.log('ingetsec');
    console.log(timeString);
    const [hour, minute, second] = timeString.split(`:`).map(Number);
    return hour * 3600 + minute * 60 + second;
  }
  getTimeStringFromSeconds(totalSeconds: number): number {
    const hour = Math.floor(totalSeconds / 3600);
    return hour;
  }
  async findAll() {
    return this.checkInRepository.find({
      relations: { user: true },
      order: { id: 'DESC' },
    });
  }
  async findAllSp() {
    const results = await this.checkInRepository.query('CALL getCheckIn()');

    return results[0];
  }
  findOne(id: number) {
    return this.checkInRepository.findOneOrFail({
      where: { id },
      relations: { user: true },
    });
  }
  findOneByUser(id: number) {
    return this.checkInRepository.findOneOrFail({
      where: { userId: id },
      relations: { user: true },
      order: { id: 'DESC' },
    });
  }
  async findAllByUserId(id: number) {
    return this.checkInRepository.find({
      where: { userId: id },
      relations: { user: true },
      order: { id: 'DESC' },
    });
  }
  async findAllInMonthByUserId(id: number, month: number) {
    const currentDate = new Date();
    month -= 1;
    console.log(month);
    currentDate.setMonth(month); // กำหนดเป็นเดือนกุมภาพันธ์ (index 1)
    console.log(currentDate.getMonth());
    const startOfMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth(),
      1,
      0,
      0,
      0,
    );
    console.log(startOfMonth.getMonth());
    const endOfMonth = new Date(
      currentDate.getFullYear(),
      currentDate.getMonth() + 1,
      0,
      23,
      59,
      59,
    );
    console.log('start : ' + startOfMonth);
    console.log('end : ' + endOfMonth);
    try {
      console.log('month');
      return await this.checkInRepository
        .createQueryBuilder('checkIn')
        .leftJoinAndSelect('checkIn.user', 'user') // โหลดข้อมูลของ user ที่เกี่ยวข้องกับ CheckIn
        .where('checkIn.userId = :id', { id }) // ตรวจสอบ userId
        .andWhere('checkIn.date BETWEEN :start AND :end', {
          start: startOfMonth.toISOString().substring(0, 10),
          end: endOfMonth.toISOString().substring(0, 10),
        }) // ตรวจสอบว่าวันที่อยู่ระหว่างเวลาเริ่มต้นและสิ้นสุดของวัน
        .getMany(); // รับข้อมูลเพียงรายการเดียวหรือข้อผิดพลาด
    } catch (error) {
      return 'ดวงจันทร์ยังคงสว่างยืมค่ำคืน';
    }
  }
  async findAllByBranchId(id: number) {
    console.log('branch');
    try {
      return await this.checkInRepository.find({
        where: {
          user: {
            branch: {
              id: id,
            },
          },
        },
        order: {
          id: 'DESC', // น้อยไปมาก
        },
        relations: { user: true },
      });
    } catch (error) {
      return 'ดวงจันทร์ยังคงสว่างชั่วค่ำคืน';
    }
  }
  async reportYear(year: string) {
    try {
      const res = await this.checkInRepository.find({
        where: {
          date: Like(`${year}-%-%`),
        },
        order: {
          id: 'DESC',
        },
        relations: { user: true },
      });

      return res;
    } catch (error) {
      return [];
    }
  }
  async reportMonth(month: string) {
    try {
      const res = await this.checkInRepository.find({
        where: {
          date: Like(`${month}-%`),
        },
        order: {
          id: 'DESC',
        },
        relations: { user: true },
      });

      return res;
    } catch (error) {
      return [];
    }
  }
  async reportDay(day: string) {
    try {
      const res = await this.checkInRepository.find({
        where: {
          date: Like(`${day}`),
        },
        order: {
          id: 'DESC',
        },
        relations: { user: true },
      });

      return res;
    } catch (error) {
      return [];
    }
  }
  async reportDayByBranch(day: string, id: number) {
    try {
      const res = await this.checkInRepository.find({
        where: {
          date: Like(`${day}`),
          user: { branch: { id: id } },
        },
        order: {
          id: 'DESC',
        },
        relations: { user: true },
      });

      return res;
    } catch (error) {
      return [];
    }
  }
  async reportYearByBranch(year: string, id: number) {
    try {
      const res = await this.checkInRepository.find({
        where: {
          date: Like(`${year}-%-%`),
          user: { branch: { id: id } },
        },
        order: {
          id: 'DESC',
        },
        relations: { user: true },
      });

      return res;
    } catch (error) {
      return [];
    }
  }
  async reportMonthByBranch(month: string, id: number) {
    try {
      const res = await this.checkInRepository.find({
        where: {
          date: Like(`${month}-%`),
          user: { branch: { id: id } },
        },
        order: {
          id: 'DESC',
        },
        relations: { user: true },
      });

      return res;
    } catch (error) {
      return [];
    }
  }
  async update(id: number, updateCheckInDto: UpdateCheckInDto) {
    const updateCheckIn = await this.checkInRepository.findOneOrFail({
      where: { id },
    });
    updateCheckIn.salary = updateCheckInDto.salary;
    updateCheckIn.status = updateCheckInDto.status;

    this.checkInRepository.save(updateCheckIn);
    return updateCheckIn;
  }

  async checkOut(id: number, updateCheckInDto: UpdateCheckInDto) {
    const updateCheckIn = await this.checkInRepository.findOneOrFail({
      where: { id },
    });
    updateCheckIn.user = updateCheckInDto.user;
    updateCheckIn.userId = updateCheckInDto.user.id;
    updateCheckIn.date = new Date(updateCheckInDto.date);
    updateCheckIn.checkIn = updateCheckInDto.checkIn;
    updateCheckIn.checkOut = updateCheckInDto.checkOut;
    const checkInSec = await this.getSecondsFromTimeString(
      updateCheckIn.checkIn,
    );
    const checkOutSec = await this.getSecondsFromTimeString(
      updateCheckIn.checkOut,
    );
    const diffSec = checkOutSec - checkInSec;

    updateCheckIn.hour = this.getTimeStringFromSeconds(diffSec);
    this.checkInRepository.save(updateCheckIn);
    return updateCheckIn;
    // const result = await this.usersRepository.findOne({
    //   where: { id },
    // });
    // return result;
  }
  async sumHour() {
    return await this.checkInRepository.query(
      'SELECT u.id,u.fullName,b.name,sum(c.hour) as total FROM check_in as c inner JOIN user as u ON u.id = c.userId inner JOIN branch as b on b.id = u.branchId GROUP BY u.id',
    );
  }
  async sumHourByDay() {
    return await this.checkInRepository.query(
      `SELECT u.id,u.fullName,b.name,sum(c.hour) as total FROM check_in as c inner JOIN user as u ON u.id = c.userId inner JOIN branch as b on b.id = u.branchId GROUP BY u.id`,
    );
  }
  async remove(id: number) {
    const deleteCheckStock = await this.checkInRepository.findOneOrFail({
      where: { id },
    });
    await this.checkInRepository.remove(deleteCheckStock);

    return deleteCheckStock;
  }
}
