import { Salary } from 'src/salary/entities/salarys.entity';
import { User } from 'src/users/entities/user.entity';
export class CreateCheckInDto {
  user: User;
  date: string;
  checkIn?: string;
  checkOut?: string;
  status?: boolean;
  salary?: Salary;
}
