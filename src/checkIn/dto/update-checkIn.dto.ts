import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckInDto } from './create-checkIn.dto';

export class UpdateCheckInDto extends PartialType(CreateCheckInDto) {}
