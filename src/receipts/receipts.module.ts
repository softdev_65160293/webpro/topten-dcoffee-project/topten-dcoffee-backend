import { Module } from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { ReceiptsController } from './receipts.controller';
import { Receipt } from './entities/receipt.entity';
import { ReceiptItem } from './entities/receiptItem.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { Product } from 'src/products/entities/product.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/member/entities/member.entity';
import { Branch } from 'src/branchs/entities/branch.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Receipt,
      ReceiptItem,
      Promotion,
      Product,
      User,
      Member,
      Branch,
    ]),
  ],
  controllers: [ReceiptsController],
  providers: [ReceiptsService],
})
export class ReceiptsModule {}
