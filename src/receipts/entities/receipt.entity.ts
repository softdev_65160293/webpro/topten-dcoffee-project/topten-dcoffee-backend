import {
  Column,
  CreateDateColumn,
  Entity,
  FindOperator,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ReceiptItem } from './receiptItem.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import { User } from 'src/users/entities/user.entity';
import { Member } from 'src/member/entities/member.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'date' }) // หรือสามารถใช้ { type: 'timestamp' } ได้ตามที่คุณต้องการ
  date: string | Date | FindOperator<Date>;

  @Column('time')
  time: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @Column()
  channel: string;

  @Column()
  qty: number;

  @Column()
  total_before: number;

  @Column({ default: 0 })
  member_discount: number;

  @Column()
  promotion_discount: number;

  @Column()
  total_discount: number;

  @Column()
  total: number;

  @Column()
  paymentType: string;

  @Column()
  change: number;

  @Column()
  receivedAmount: number;

  @Column()
  branchId?: number;

  @Column({ default: null })
  userId?: number;

  @Column({ default: null })
  memberId?: number;

  @OneToMany(() => ReceiptItem, (receiptItem) => receiptItem.receipt, {
    cascade: true,
  })
  receiptItems: ReceiptItem[];

  @ManyToMany(() => Promotion, (promotion) => promotion.receipts, {
    cascade: true,
  })
  @JoinTable()
  promotions?: Promotion[];

  @ManyToOne(() => Branch, (branch) => branch.receipts)
  branch: Branch;

  @ManyToOne(() => User, (user) => user.receipts)
  user: User;

  @ManyToOne(() => Member, (member) => member.receipts)
  member?: Member;
  receipt: Promise<Branch>;
}
