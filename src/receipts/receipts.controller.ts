import {
  Controller,
  Get,
  Post,
  Body,
  // Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ReceiptsService } from './receipts.service';
import { CreateReceiptDto } from './dto/create-receipt.dto';
// import { UpdateReceiptDto } from './dto/update-receipt.dto';
// import { AuthGuard } from 'src/auth/auth.guard';

// @UseGuards(AuthGuard)
@Controller('receipts')
export class ReceiptsController {
  constructor(private readonly receiptsService: ReceiptsService) { }

  @Post()
  create(@Body() createReceiptDto: CreateReceiptDto) {
    return this.receiptsService.create(createReceiptDto);
  }

  @Get()
  findAll() {
    return this.receiptsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.receiptsService.findOne(+id);
  }

  @Get('branch/:id')
  findByBranch(@Param('id') id: string) {
    console.log('coll');
    return this.receiptsService.findByBranch(+id);
  }

  @Get('report/:type/:price')
  findReport(@Param('type') type: string, @Param('price') price: string) {
    console.log('controll');
    return this.receiptsService.findReport(type, +price);
  }

  @Get('/sales/day/:id/:month')
  salesDaily(@Param('id') id: number, @Param('month') month: string) {
    return this.receiptsService.salesDaily(id, month);
  }

  @Get('/sales/month/:id')
  salesMonth(@Param('id') id: number) {
    return this.receiptsService.salesMonth(id);
  }

  @Get('/sales/year/:id')
  salesYear(@Param('id') id: number) {
    return this.receiptsService.salesYear(id);
  }

  @Get('report/:branchId')
  reportYearByBranch(@Param('branchId') branchId: string) {
    return this.receiptsService.reportYearByBranch(branchId);
  }

  @Get('owner/:year')
  reportYear(@Param('year') year: string) {
    return this.receiptsService.reportYear(year);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.receiptsService.remove(+id);
  }

  @Get('memberId/:id')
  findBymember(@Param('id') id: number) {
    console.log('coll');
    return this.receiptsService.findBymember(id);
  }

  // @Get('staffId/:id')
  // findBystaff(@Param('id') id: number) {
  //   console.log('coll');
  //   return this.receiptsService.findBystaff(id);
  // }
}
