import { Branch } from 'src/branchs/entities/branch.entity';
import { Member } from 'src/member/entities/member.entity';
import { Promotion } from 'src/promotions/entities/promotion.entity';
import { User } from 'src/users/entities/user.entity';

export class CreateReceiptDto {
  receiptItems: {
    productId: number;
    unit: number;
    sweet_level: string;
    gsize: string;
    type: string;
  }[];
  branch: Branch;
  branchId: number;
  user: User;
  member: Member;
  promotions?: Promotion[];
  date: string;
  time: string;
  channel: string;
  paymentType: string;
  receivedAmount: number;
  change: number;
  total_discount: number;
  promotion_discount: number;
  member_discount: number;
  total_before: number;
}
