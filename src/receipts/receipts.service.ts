import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
// import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { ReceiptItem } from './entities/receiptItem.entity';
import { Product } from 'src/products/entities/product.entity';
import { Member } from 'src/member/entities/member.entity';
import { Branch } from 'src/branchs/entities/branch.entity';

@Injectable()
export class ReceiptsService {
  constructor(
    @InjectRepository(Receipt) private receiptsRepository: Repository<Receipt>,
    @InjectRepository(Member) private membersRepository: Repository<Member>,
    @InjectRepository(Branch) private branchsRepository: Repository<Branch>,
    @InjectRepository(ReceiptItem)
    private receiptItemsRepository: Repository<ReceiptItem>,
    @InjectRepository(Product) private productsRepository: Repository<Product>,
  ) { }
  async create(createReceiptDto: CreateReceiptDto) {
    try {
      const receipt = new Receipt();
      // receipt.staffId = 0;
      receipt.memberId = 0;
      receipt.receiptItems = [];
      receipt.branchId = 1;
      receipt.qty = 0;
      receipt.total = 0;
      receipt.total_before = 0;
      receipt.total_discount = 0;
      receipt.promotion_discount = 0;
      receipt.member_discount = 0;
      const branch = await this.branchsRepository.findOneBy({
        id: createReceiptDto.branchId,
      });
      console.log('BRANCH LOG' + branch);

      receipt.branch = branch;
      console.log('just');
      if (createReceiptDto.member) {
        receipt.member = createReceiptDto.member;
      } else {
        receipt.member = null;
      }
      if (createReceiptDto.promotions) {
        receipt.promotions = createReceiptDto.promotions;
      } else {
        receipt.promotions = null;
      }

      if (createReceiptDto.user) {
        receipt.user = createReceiptDto.user;
      } else {
        receipt.user = null;
      }
      console.log('promotion');
      receipt.member_discount = createReceiptDto.member_discount;
      receipt.promotion_discount = createReceiptDto.promotion_discount;
      receipt.channel = createReceiptDto.channel;
      receipt.change = createReceiptDto.change;
      receipt.receivedAmount = createReceiptDto.receivedAmount;
      receipt.date = new Date(createReceiptDto.date);
      receipt.time = createReceiptDto.time;
      receipt.paymentType = createReceiptDto.paymentType;
      console.log('other');
      for (const io of createReceiptDto.receiptItems) {
        const receiptItem = new ReceiptItem();
        receiptItem.product = await this.productsRepository.findOneBy({
          id: io.productId,
        });
        receiptItem.name = receiptItem.product.name;
        receiptItem.price = receiptItem.product.price;
        receiptItem.unit = io.unit;
        receiptItem.sweet_level = io.sweet_level;
        receiptItem.type = io.type;
        receiptItem.gsize = io.gsize;
        receiptItem.total = receiptItem.price * receiptItem.unit;
        await this.receiptItemsRepository.save(receiptItem);
        receipt.receiptItems.push(receiptItem);
        receipt.qty += receiptItem.unit;
        receipt.total_before += receiptItem.total;
      }
      console.log('receiptItem');
      receipt.promotion_discount = createReceiptDto.promotion_discount;
      receipt.total_discount = createReceiptDto.total_discount;
      receipt.total = receipt.total_before - receipt.total_discount;
      console.log(receipt);
      return this.receiptsRepository.save(receipt);
    } catch (e) { }
  }

  findAll() {
    return this.receiptsRepository.find({
      relations: {
        receiptItems: true,
        promotions: true,
        user: true,
        member: true,
        branch: true,
      },
    });
  }
  async findReport(type: string, price: number) {
    console.log('service');
    const res = await this.receiptsRepository.query(
      `CALL reportsell('${type}', ${price})`,
    );
    return res[0];
  }

  findOne(id: number) {
    return this.receiptsRepository.find({
      where: { id: id },
      relations: { receiptItems: true, promotions: true },
    });
  }

  findBymember(id: number) {
    console.log('service');
    return this.receiptsRepository.find({
      where: { memberId: id },
      relations: {
        member: true,
      },
    });
  }

  // findBystaff(id: number) {
  //   console.log('service');
  //   return this.receiptsRepository.find({
  //     where: { staffId: id },
  //     // relations: {
  //     //   staff: true,
  //     // },
  //   });
  // }

  async reportYear(year: string) {
    const result = await this.receiptsRepository.query(
      `SELECT YEAR(calendar.month_year) AS year,
                MONTH(calendar.month_year) AS month,
                COALESCE(SUM(A.total), 0) AS total_month
         FROM
            (SELECT '${year}-01-01' AS month_year
             UNION SELECT '${year}-02-01'
             UNION SELECT '${year}-03-01'
             UNION SELECT '${year}-04-01'
             UNION SELECT '${year}-05-01'
             UNION SELECT '${year}-06-01'
             UNION SELECT '${year}-07-01'
             UNION SELECT '${year}-08-01'
             UNION SELECT '${year}-09-01'
             UNION SELECT '${year}-10-01'
             UNION SELECT '${year}-11-01'
             UNION SELECT '${year}-12-01') AS calendar
         LEFT JOIN receipt A ON MONTH(A.Date) = MONTH(calendar.month_year)
                                    AND YEAR(A.Date) = YEAR(calendar.month_year)
         GROUP BY YEAR(calendar.month_year), MONTH(calendar.month_year)
         ORDER BY YEAR(calendar.month_year), MONTH(calendar.month_year);`,
    );
    return result;
  }

  async reportYearByBranch(branchId: string) {
    const year = new Date().getFullYear();
    const result = await this.receiptsRepository.query(
      `SELECT YEAR(calendar.month_year) AS year,
            MONTH(calendar.month_year) AS month,
            COALESCE(SUM(A.total), 0) AS total_month
      FROM
      (SELECT '${year}-01-01' AS month_year
      UNION SELECT '${year}-02-01'
      UNION SELECT '${year}-03-01'
      UNION SELECT '${year}-04-01'
      UNION SELECT '${year}-05-01'
      UNION SELECT '${year}-06-01'
      UNION SELECT '${year}-07-01'
      UNION SELECT '${year}-08-01'
      UNION SELECT '${year}-09-01'
      UNION SELECT '${year}-10-01'
      UNION SELECT '${year}-11-01'
      UNION SELECT '${year}-12-01') AS calendar
      LEFT JOIN receipt A ON MONTH(A.Date) = MONTH(calendar.month_year)
                                AND YEAR(A.Date) = YEAR(calendar.month_year)
                                AND A.branchId = ${branchId}
      GROUP BY YEAR(calendar.month_year), MONTH(calendar.month_year)
      ORDER BY YEAR(calendar.month_year), MONTH(calendar.month_year);`,
    );
    console.log(result);
    return result;
  }

  async reportYears(selectedYear: string) {
    console.log('REPORT Year FUNCTION');
    console.log('Years ' + selectedYear);
    const result = await this.receiptsRepository.query(
      `SELECT YEAR(calendar.month_year) AS year,
                MONTH(calendar.month_year) AS month,
                COALESCE(SUM(A.total), 0) AS total_month
        FROM
            (SELECT '${selectedYear}-01-01' AS month_year
            UNION SELECT '${selectedYear}-02-01'
            UNION SELECT '${selectedYear}-03-01'
            UNION SELECT '${selectedYear}-04-01'
            UNION SELECT '${selectedYear}-05-01'
            UNION SELECT '${selectedYear}-06-01'
            UNION SELECT '${selectedYear}-07-01'
            UNION SELECT '${selectedYear}-08-01'
            UNION SELECT '${selectedYear}-09-01'
            UNION SELECT '${selectedYear}-10-01'
            UNION SELECT '${selectedYear}-11-01'
            UNION SELECT '${selectedYear}-12-01') AS calendar
        LEFT JOIN receipt A ON MONTH(A.Date) = MONTH(calendar.month_year)
                                    AND YEAR(A.Date) = YEAR(calendar.month_year)
        GROUP BY YEAR(calendar.month_year), MONTH(calendar.month_year)
        ORDER BY YEAR(calendar.month_year), MONTH(calendar.month_year);`,
    );
    console.log(result);
    return result;
  }

  async salesDaily(id: number, month: string) {
    console.log('Daily sale');
    const result = await this.receiptsRepository
      .query(`SELECT DATE_FORMAT(r.date, '%d') AS Day, 
      SUM(r.total) AS Total, 
      MONTH(r.date) AS Month,
      u.fullName AS Full_Name, 
      u.branchId AS Branch
      FROM receipt AS r 
      LEFT JOIN user AS u ON r.userId = u.id 
      WHERE r.userId IS NOT NULL and u.id = ${id} and MONTH(r.date) = ${month}
      GROUP BY DATE_FORMAT(r.date, '%d'), MONTH(r.date), u.fullName;
      `);
    console.log(result);
    return result;
  }

  async salesMonth(id: number) {
    console.log('Month sale');
    const result = await this.receiptsRepository
      .query(`SELECT DATE_FORMAT(r.date, '%m') AS Month, SUM(r.total) AS Total, u.fullName AS Full_Name, u.branchId AS Branch,DATE_FORMAT(r.date, '%y') As Year
      FROM receipt AS r 
      LEFT JOIN user AS u ON r.userId = u.id
      WHERE r.userId IS NOT NULL and u.id = ${id} and DATE_FORMAT(r.date, '%y') = "24"
      GROUP BY DATE_FORMAT(r.date, '%y') ASC,DATE_FORMAT(r.date, '%m'), u.fullName;`);
    console.log(result);
    return result;
  }

  async salesYear(id: number) {
    console.log('Year sale');
    const result = await this.receiptsRepository
      .query(`SELECT DATE_FORMAT(r.date, '%y') AS Year, SUM(r.total) AS Total, u.fullName AS Full_Name, u.branchId AS Branch
      FROM receipt AS r 
      LEFT JOIN user AS u ON r.userId = u.id
      WHERE r.userId IS NOT NULL and u.id = ${id}
      GROUP BY DATE_FORMAT(r.date, '%y'), u.fullName;
      `);
    console.log(result);
    return result;
  }

  findByBranch(id: number) {
    console.log('service');
    return this.receiptsRepository.find({
      where: { branchId: id },
      relations: {
        receiptItems: true,
        promotions: true,
        user: true,
        branch: true,
      },
    });
  }

  remove(id: number) {
    return this.receiptsRepository.delete(id);
  }
}
