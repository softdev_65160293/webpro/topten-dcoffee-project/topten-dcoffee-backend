import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';

@Injectable()
export class BranchsService {
  constructor(
    @InjectRepository(Branch) private branchRepository: Repository<Branch>,
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}
  async create(createBranchDto: CreateBranchDto) {
    const branch = new Branch();
    const manager = await this.usersRepository.findOneBy({
      id: createBranchDto.managerId,
    });
    branch.name = createBranchDto.name;
    branch.Address = createBranchDto.Address;
    branch.tel = createBranchDto.tel;
    branch.manager = manager.fullName;
    return this.branchRepository.save(branch);
  }

  findAll() {
    return this.branchRepository.find();
  }

  findOne(id: number) {
    return this.branchRepository.findOne({ where: { id } });
  }

  findOneByName(name: string) {
    return this.branchRepository.findOne({ where: { name } });
  }

  async update(id: number, updateBranchDto: UpdateBranchDto) {
    const branch = new Branch();
    const manager = await this.usersRepository.findOneBy({
      id: updateBranchDto.managerId,
    });
    branch.name = updateBranchDto.name;
    branch.Address = updateBranchDto.Address;
    branch.tel = updateBranchDto.tel;
    branch.manager = manager.fullName;
    return this.branchRepository.update(id, branch);
  }

  remove(id: number) {
    return `This action removes a #${id} branch`;
  }
}
