//import { Billcost } from 'src/billcosts/entities/billcost.entity';
import { Billcost } from 'src/billcosts/entities/billcost.entity';
import { Checkstock } from 'src/checkstocks/entities/checkstock.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { ReceiptStock } from 'src/receiptstocks/entities/receiptstock.entity';
import { Stock } from 'src/stocks/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  Address: string;

  @Column()
  tel: string;

  @Column({
    default: '',
  })
  manager: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => User, (user) => user.branch)
  user: User[];

  @OneToMany(() => Billcost, (billcost) => billcost.branch)
  billcosts: Billcost[];

  @OneToMany(() => Stock, (stock) => stock.branch)
  stocks: Stock[];

  // @OneToMany(() => Receipt, (receipt) => receipt.branch)
  // receipts: Receipt[];

  // @OneToMany(() => Receipt, (receipt) => receipt.branch)
  // receipts: Receipt[];

  @OneToMany(() => ReceiptStock, (receipt) => receipt.branch)
  receiptStock: ReceiptStock[];

  @OneToMany(() => Checkstock, (checkStock) => checkStock.branch)
  checkStock: Checkstock[];

  @OneToMany(() => Receipt, (receipt) => receipt.branch)
  receipts: Receipt[];
}
