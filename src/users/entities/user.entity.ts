import { Billcost } from 'src/billcosts/entities/billcost.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import { CheckIn } from 'src/checkIn/entities/checkIn.entity';
import { Checkstock } from 'src/checkstocks/entities/checkstock.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
import { ReceiptStock } from 'src/receiptstocks/entities/receiptstock.entity';
import { Salary } from 'src/salary/entities/salarys.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column({
    default: 'user',
  })
  username: string;

  @Column()
  password: string;

  @Column({
    name: 'fullName',
    default: '',
  })
  fullName: string;

  @Column({
    name: 'role',
    default: 'Admin',
  })
  role: string;

  @Column()
  gender: string;

  @Column({ default: 'noimg.jpg' })
  image: string;

  @Column({
    default: 0,
  })
  workRate: number;

  @Column({
    default: 'PT',
  })
  workType: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(() => ReceiptStock, (receiptStock) => receiptStock.user)
  receiptStock: ReceiptStock[];

  @OneToMany(() => CheckIn, (checkIn) => checkIn.user)
  checkIns: CheckIn[];

  @OneToMany(() => Checkstock, (checkStock) => checkStock.user)
  checkStock: Checkstock[];

  @ManyToOne(() => Branch, (branch) => branch.user)
  branch: Branch;

  @OneToMany(() => Billcost, (billcost) => billcost.user)
  billcosts: Billcost[];

  @OneToMany(() => Receipt, (receipt) => receipt.user)
  receipts: Receipt[];

  @OneToMany(() => Salary, (Salary) => Salary.user)
  salarys: Salary[];
}
