import { Branch } from 'src/branchs/entities/branch.entity';

export class CreateUserDto {
  id: number;
  email: string;
  username: string;
  password: string;
  fullName: string;
  gender: string;
  role: string;
  image: string;
  branch: Branch;
  branchId: number;
  workRate: string;
  workType: string;
}
