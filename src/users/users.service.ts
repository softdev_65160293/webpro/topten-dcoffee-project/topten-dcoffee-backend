import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Branch } from 'src/branchs/entities/branch.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Branch) private branchRepository: Repository<Branch>,
  ) {}
  async create(createUserDto: CreateUserDto) {
    console.log(createUserDto.image);
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.gender = createUserDto.gender;
    user.username = createUserDto.username;
    user.password = createUserDto.password;
    user.workRate = parseFloat(createUserDto.workRate);
    user.workType = createUserDto.workType;
    user.role = createUserDto.role;
    if (createUserDto.branchId) {
      user.branch = await this.branchRepository.findOneOrFail({
        where: { id: createUserDto.branchId },
      });
    }
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }
    console.log(createUserDto);
    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({ relations: { branch: true } });
  }

  async findUserLoop() {
    const res = await this.usersRepository.query(`CALL loopUser();`);
    return res[0];
  }
  findAllByBranch(branchId: number) {
    return this.usersRepository.find({
      where: { branch: { id: branchId } },
      relations: { branch: true },
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { branch: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneOrFail({
      where: { email },
      relations: { branch: true },
    });
  }

  findOneByUsername(username: string) {
    return this.usersRepository.findOneOrFail({
      where: { username },
      relations: { branch: true },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.gender = updateUserDto.gender;
    user.username = updateUserDto.username;
    user.password = updateUserDto.password;
    user.workRate = parseFloat(updateUserDto.workRate);
    user.workType = updateUserDto.workType;
    user.role = updateUserDto.role;
    if (updateUserDto.branchId) {
      user.branch = await this.branchRepository.findOneOrFail({
        where: { id: updateUserDto.branchId },
      });
    }
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }

    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });

    updateUser.email = user.email;
    updateUser.username = user.username;
    console.log(updateUser.username);
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image;
    updateUser.workRate = user.workRate;
    updateUser.workType = user.workType;
    updateUser.role = user.role;
    updateUser.branch = user.branch;
    this.usersRepository.save(updateUser);

    const result = await this.usersRepository.findOne({
      where: { id },
      relations: {
        branch: true,
      },
    });
    console.log(result);
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteUser);

    return deleteUser;
  }
  async insertUser(createDto: CreateUserDto) {
    createDto.branchId = 1;
    try {
      // Execute CALL statement
      await this.usersRepository.query(`
        CALL insertUser('${createDto.email}', '${createDto.username}', '${createDto.password}', '${createDto.fullName}', '${createDto.role}', '${createDto.gender}', '${createDto.image}', ${createDto.branchId}, @p8, @p9);
      `);

      // Execute SELECT statement
      const result = await this.usersRepository.query(`
        SELECT @p8 AS xResult, @p9 AS xMessage;
      `);

      return result[0];
    } catch (error) {
      console.log(error);
      // Handle error
    }
  }

  async checkUser(createDto: CreateUserDto) {
    try {
      // Execute CALL statement
      console.log(createDto);
      const result = await this.usersRepository.query(`
        CALL loopUser('${createDto.email}', '${createDto.username}', '${createDto.password}', '${createDto.fullName}', '${createDto.role}', '${createDto.gender}', '${createDto.image}', ${createDto.branchId},${createDto.workRate},'${createDto.workType}')`);
      return result[0];
    } catch (error) {
      console.log(error);
      // Handle error
    }
  }
  async viewUserSell() {
    console.log('service');
    const result = await this.usersRepository.query(
      'SELECT * FROM viewemployeesell',
    );
    return result;
  }
}
// import { Injectable } from '@nestjs/common';
// import { CreateUserDto } from './dto/create-user.dto';
// import { UpdateUserDto } from './dto/update-user.dto';
// import { Repository } from 'typeorm';
// import { User } from './entities/user.entity';
// import { InjectRepository } from '@nestjs/typeorm';
// import { Role } from 'src/roles/entities/role.entity';

// @Injectable()
// export class UsersService {
//   findAll() {
//     return this.usersRepository.find();
//   }
//   async findOne(id: number): Promise<User> {
//     return await this.usersRepository.findOne({ where: { id } });
//   }

//   constructor(
//     @InjectRepository(User) private usersRepository: Repository<User>,
//     @InjectRepository(Role) private rolesRepository: Repository<Role>,
//   ) {}

//   async create(createUserDto: CreateUserDto): Promise<User> {
//     const user = new User();
//     Object.assign(user, createUserDto);
//     if (createUserDto.image && createUserDto.image !== '') {
//       user.image = createUserDto.image;
//     }
//     return await this.usersRepository.save(user);
//   }

//   async findAdminsAndUsers(): Promise<User[]> {
//     return await this.usersRepository.find({
//       where: [{ role: 'admin' }, { role: 'user' }],
//     });
//   }

//   async findOneById(id: number): Promise<User> {
//     return await this.usersRepository.findOne({ where: { id } });
//   }
//   async findOneByEmail(email: string): Promise<User> {
//     return await this.usersRepository.findOne({
//       where: { email },
//     });
//   }

//   async update(id: number, updateUserDto: UpdateUserDto): Promise<User> {
//     const user = await this.usersRepository.findOneOrFail({ where: { id } });
//     Object.assign(user, updateUserDto);
//     if (updateUserDto.image && updateUserDto.image !== '') {
//       user.image = updateUserDto.image;
//     }
//     const updatedUser = await this.usersRepository.save(user);
//     return updatedUser;
//   }
//   async remove(id: number): Promise<User> {
//     const user = await this.usersRepository.findOneOrFail({ where: { id } });
//     await this.usersRepository.remove(user);
//     return user;
//   }
// }
