import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';
import { Branch } from 'src/branchs/entities/branch.entity';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      console.log(file);
      createUserDto.image = file.filename;
    }
    return this.usersService.create(createUserDto);
  }
  @Post('insertUser')
  InsertUser(@Body() create: CreateUserDto) {
    return this.usersService.insertUser(create);
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }
  @Get('view')
  viewUserSell() {
    console.log('view');
    return this.usersService.viewUserSell();
  }

  @Post('checkUser')
  findUserLoop(@Body() create: CreateUserDto) {
    console.log('while');
    return this.usersService.checkUser(create);
  }

  @Get(':id')
  @Get('user/:id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(+id);
  }

  @Get('branch/:branchId')
  findAllByBranch(@Param('branchId') branchId: string) {
    return this.usersService.findAllByBranch(+branchId);
  }
  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateUserDto.image = file.filename;
    }
    console.log(updateUserDto);
    return this.usersService.update(+id, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(+id);
  }

  @Post('upload')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/users',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  uploadFile(
    @Body()
    user: {
      name: string;
      email: string;
      password: string;
      fullName: string;
      role: string;
      gender: string;
      branch: Branch;
    },
    @UploadedFile() file: Express.Multer.File,
  ) {
    console.log(user);
    console.log(file.filename);
    console.log(file.path);
  }
}
