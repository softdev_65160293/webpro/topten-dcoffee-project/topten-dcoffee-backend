import { Injectable } from '@nestjs/common';
import { CreateBillcostDto } from './dto/create-billcost.dto';
import { UpdateBillcostDto } from './dto/update-billcost.dto';
import { Billcost } from './entities/billcost.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Typebillcost } from 'src/typebillcosts/entities/typebillcost.entity';

@Injectable()
export class BillcostsService {
  constructor(
    @InjectRepository(Billcost)
    private billcostsRepository: Repository<Billcost>,
  ) {}
  create(createBillcostDto: CreateBillcostDto) {
    const billcost = new Billcost();
    billcost.typebillcost = JSON.parse(createBillcostDto.typebillcost);
    billcost.billtotal = parseFloat(createBillcostDto.billtotal);
    billcost.user = JSON.parse(createBillcostDto.user);
    billcost.date = new Date(createBillcostDto.date);
    billcost.time = new Date(
      `${createBillcostDto.date} ${createBillcostDto.time}`,
    );

    billcost.branch = JSON.parse(createBillcostDto.branch);
    if (createBillcostDto.image && createBillcostDto.image !== '') {
      billcost.image = createBillcostDto.image;
    }
    return this.billcostsRepository.save(billcost);
  }

  async reportTotalCostByType() {
    console.log('REPORT FUNCTION');
    const result = await this.billcostsRepository.query(
      `CALL CalculateTotalCostByType`,
    );
    console.log(result);
    return result[0];
  }

  async reportTotalcostbytypeandbranc(
    TypebillcostId: string,
    branchId: string,
  ) {
    console.log('REPORT FUNCTION');
    const result = await this.billcostsRepository.query(
      `CALL CalculateTotalCostByTypeAndBranch(${TypebillcostId}, '${branchId}')`,
    );
    console.log(result);
    return result[0];
  }

  async reportBillTotalByBranchAndTypePerYear(branchId: number, year: number) {
    console.log('REPORT FUNCTION');
    console.log(branchId);
    console.log(year);
    const result = await this.billcostsRepository.query(
      `
      SELECT
          b.branchId,
          CONCAT(tbc.name, ' ', MONTHNAME(b.date)) AS typebillcost_month,
          SUM(b.billtotal) AS total_cost
      FROM
          Billcost b
      INNER JOIN
          Typebillcost tbc ON b.TypebillcostId = tbc.id
      WHERE
          YEAR(b.date) = ${year} and b.branchId=${branchId}
      GROUP BY
          b.branchId,
          typebillcost_month
      ORDER BY
          b.branchId,
          MONTH(b.date);`,
    );
    console.log(result);
    return result;
  }

  async reportBillTotalByBranchAndTypePerMonth(branchId: number) {
    console.log('REPORT FUNCTION');
    const result = await this.billcostsRepository.query(
      `SELECT
      b.branchId,
      CONCAT(tbc.name, ' ', MONTHNAME(b.date), ' ', YEAR(b.date)) AS typebillcost_month,
      SUM(b.billtotal) AS total_cost
  FROM
      Billcost b
  INNER JOIN
      Typebillcost tbc ON b.TypebillcostId = tbc.id
  WHERE
      b.branchId =${branchId}
  GROUP BY
      b.branchId,
      typebillcost_month
  ORDER BY
      b.branchId,
      YEAR(b.date),
      MONTH(b.date);
  ;
  `,
    );
    console.log(result);
    return result;
  }

  async reportTotalCostByUserAndType() {
    console.log('REPORT FUNCTION');
    const result = await this.billcostsRepository.query(
      `CALL CalculateTotalCostByUserAndType`,
    );
    console.log(result);
    return result[0];
  }

  findAll() {
    return this.billcostsRepository.find({
      relations: ['user', 'branch', 'typebillcost'],
    });
  }

  findAllByTypebillcost(typebillcostId: number) {
    return this.billcostsRepository.find({
      where: { typebillcost: { id: typebillcostId } },
      relations: ['user', 'branch', 'typebillcost'],
      // order: { created: 'ASC' },
    });
  }

  findAllByUser(userId: number) {
    return this.billcostsRepository.find({
      where: { user: { id: userId } },
      relations: ['user', 'branch', 'typebillcost'],
      // order: { created: 'ASC' },
    });
  }

  findAllByBranch(branchId: number) {
    return this.billcostsRepository.find({
      where: { branch: { id: branchId } },
      relations: ['user', 'branch', 'typebillcost'],
      // order: { created: 'ASC' },
    });
  }

  findAllByFullName(fullName: string) {
    return this.billcostsRepository.find({
      where: { user: { fullName } },
      relations: ['user', 'branch', 'typebillcost'],
    });
  }

  findOne(id: number) {
    return this.billcostsRepository.findOne({
      where: { id },
      relations: ['user', 'branch', 'typebillcost'],
    });
  }

  async update(id: number, updateBillcostDto: UpdateBillcostDto) {
    const billcost = await this.billcostsRepository.findOneOrFail({
      where: { id },
    });
    billcost.typebillcost = JSON.parse(updateBillcostDto.typebillcost);
    billcost.billtotal = parseFloat(updateBillcostDto.billtotal);
    billcost.user = JSON.parse(updateBillcostDto.user);
    billcost.date = new Date(updateBillcostDto.date);
    billcost.time = new Date(
      `${updateBillcostDto.date} ${updateBillcostDto.time}`,
    );
    billcost.branch = JSON.parse(updateBillcostDto.branch);
    if (updateBillcostDto.image && updateBillcostDto.image !== '') {
      billcost.image = updateBillcostDto.image;
    }
    this.billcostsRepository.save(billcost);
    return billcost;
  }

  async remove(id: number) {
    const deleteBillCost = await this.billcostsRepository.findOneOrFail({
      where: { id },
    });
    await this.billcostsRepository.remove(deleteBillCost);

    return deleteBillCost;
  }
}
