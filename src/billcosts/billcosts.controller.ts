import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  UseGuards,
} from '@nestjs/common';
import { BillcostsService } from './billcosts.service';
import { CreateBillcostDto } from './dto/create-billcost.dto';
import { UpdateBillcostDto } from './dto/update-billcost.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { uuid } from 'uuidv4';
import { extname } from 'path';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('billcosts')
export class BillcostsController {
  constructor(private readonly billcostsService: BillcostsService) {}

  @Get('/reportTotalCostByType')
  reportTotalCostByType() {
    return this.billcostsService.reportTotalCostByType();
  }

  @Get('/reportTotalCostByUserAndType')
  reportTotalCostByUserAndType() {
    return this.billcostsService.reportTotalCostByUserAndType();
  }

  @Get('/reportTotalcostbytypeandbranc/:TypebillcostId/:branchId')
  reportTotalcostbytypeandbranc(
    @Param('TypebillcostId') TypebillcostId: string,
    @Param('branchId') branchId: string,
  ) {
    return this.billcostsService.reportTotalcostbytypeandbranc(
      TypebillcostId,
      branchId,
    );
  }

  @Get('/GetBillTotalByBranchAndTypePerYear/:branchId/:year')
  reportBillTotalByBranchAndTypePerYear(
    @Param('branchId') branchId: number,
    @Param('year') year: number,
  ) {
    return this.billcostsService.reportBillTotalByBranchAndTypePerYear(
      branchId,
      year,
    );
  }

  @Get('/GetBillTotalByBranchAndTypePerMonth/:branchId')
  reportBillTotalByBranchAndTypePerMonth(@Param('branchId') branchId: number) {
    return this.billcostsService.reportBillTotalByBranchAndTypePerMonth(
      branchId,
    );
  }

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/billcosts',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createBillcostDto: CreateBillcostDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      createBillcostDto.image = file.filename;
    }
    return this.billcostsService.create(createBillcostDto);
  }

  @Get()
  findAll() {
    return this.billcostsService.findAll();
  }

  @Get('Typebillcost/:TypebillcostId')
  findAllByTypebillcost(@Param('TypebillcostId') TypebillcostId: number) {
    return this.billcostsService.findAllByTypebillcost(TypebillcostId);
  }

  @Get('user/:userId')
  findAllByUser(@Param('userId') userId: number) {
    return this.billcostsService.findAllByUser(userId);
  }

  @Get('user/:fullName')
  findAllByFullName(@Param('fullName') fullName: string) {
    return this.billcostsService.findAllByFullName(fullName);
  }

  @Get('branch/:branchId')
  findAllByBranch(@Param('branchId') branchId: number) {
    return this.billcostsService.findAllByBranch(branchId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.billcostsService.findOne(+id);
  }

  @Post(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './public/images/billcosts',
        filename: (req, file, cb) => {
          const name = uuid();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  update(
    @Param('id') id: string,
    @Body() updateBillcostDto: UpdateBillcostDto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    if (file) {
      updateBillcostDto.image = file.filename;
    }
    return this.billcostsService.update(+id, updateBillcostDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.billcostsService.remove(+id);
  }
}
