import { Branch } from 'src/branchs/entities/branch.entity';
import { Typebillcost } from 'src/typebillcosts/entities/typebillcost.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

@Entity()
export class Billcost {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Typebillcost, (typebillcost) => typebillcost.Typebill)
  @JoinColumn({ name: 'TypebillcostId' })
  typebillcost: Typebillcost[];

  @Column()
  billtotal: number;

  @ManyToOne(() => User, (user) => user.billcosts)
  user: User;

  @Column({ type: 'date' }) //YYYY-MM-DD
  date: Date;

  @Column({ type: 'time' }) //HH:MM:SS
  time: Date;

  @ManyToOne(() => Branch, (branch) => branch.billcosts)
  @JoinColumn({ name: 'branchId' })
  branch: Branch;

  @Column({ default: 'noimg.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;
}
