import { Test, TestingModule } from '@nestjs/testing';
import { BillcostsController } from './billcosts.controller';
import { BillcostsService } from './billcosts.service';

describe('BillcostsController', () => {
  let controller: BillcostsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BillcostsController],
      providers: [BillcostsService],
    }).compile();

    controller = module.get<BillcostsController>(BillcostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
