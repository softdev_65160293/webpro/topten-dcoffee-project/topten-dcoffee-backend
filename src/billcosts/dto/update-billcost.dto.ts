import { PartialType } from '@nestjs/mapped-types';
import { CreateBillcostDto } from './create-billcost.dto';

export class UpdateBillcostDto extends PartialType(CreateBillcostDto) {}
