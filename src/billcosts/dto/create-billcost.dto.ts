export class CreateBillcostDto {
  typebillcost: string;
  billtotal: string;
  user: string;
  date: Date;
  time: Date;
  branch: string;
  image: string;
}
