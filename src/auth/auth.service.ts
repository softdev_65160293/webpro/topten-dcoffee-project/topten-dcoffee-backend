import { Injectable, UnauthorizedException } from '@nestjs/common'; //, UnauthorizedException
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { MembersService } from 'src/member/members.service';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private membersService: MembersService,
    private jwtService: JwtService,
  ) {}

  async signIn(username: string, pass: string): Promise<any> {
    try {
      let user;
      let res;
      try {
        res = await this.usersService.findOneByUsername(username);
      } catch (e) {
        res = null;
      }

      if (res === null) {
        user = await this.membersService.findOneByUsername(username);
      } else if (res !== null) {
        user = res;
      }

      if (user?.password !== pass) {
        throw new UnauthorizedException();
      }
      const payload = { sub: user.id, username: user.username };
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return {
        user: result,
        access_token: await this.jwtService.signAsync(payload),
      };
    } catch (e) {
      throw new UnauthorizedException();
    }
  }
}
