import {
  Column,
  CreateDateColumn,
  Entity,
  FindOperator,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Product } from 'src/products/entities/product.entity';
import { Receipt } from 'src/receipts/entities/receipt.entity';
// import { Receipt } from 'src/receipts/entities/receipt.entity';
@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  condition: string;

  @Column({ type: 'float' })
  discount: number;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column({ type: 'date' })
  start: string | Date | FindOperator<Date>;

  @Column({ type: 'date' })
  end: string | Date | FindOperator<Date>;

  @Column()
  status: boolean;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Product, (product) => product.promotions, { cascade: true })
  @JoinTable()
  products: Product[];

  @ManyToMany(() => Receipt, (receipt) => receipt.promotions)
  receipts: Receipt[];
}
