import { Injectable } from '@nestjs/common';
import { CreateCheckstockDto } from './dto/create-checkstock.dto';
import { UpdateCheckstockDto } from './dto/update-checkstock.dto';
import { Checkstock } from './entities/checkstock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Stock } from 'src/stocks/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import { CheckStockItem } from './entities/checkstockItems.entity';
import { Branch } from 'src/branchs/entities/branch.entity';

@Injectable()
export class CheckstocksService {
  @InjectRepository(Checkstock)
  private checkStockRepository: Repository<Checkstock>;
  @InjectRepository(Stock) private stocksRepository: Repository<Stock>;
  @InjectRepository(User) private usersRepository: Repository<User>;
  @InjectRepository(Branch) private branchsRepository: Repository<Branch>;
  @InjectRepository(CheckStockItem)
  private checkStockItemRepository: Repository<CheckStockItem>;
  async create(createCheckstockDto: CreateCheckstockDto) {
    console.log('in create');
    const checkstock = new Checkstock();

    const user = await this.usersRepository.findOneBy({
      id: createCheckstockDto.userId,
    });
    const branch = await this.branchsRepository.findOneBy({
      id: createCheckstockDto.branchId,
    });
    checkstock.user = user;
    checkstock.branch = branch;
    checkstock.date = new Date(createCheckstockDto.date);
    checkstock.checkStockItems = [];
    for (const i of createCheckstockDto.checkStockItems) {
      const checkStockItem = new CheckStockItem();
      const stock = new Stock();
      checkStockItem.stock = await this.stocksRepository.findOneBy({
        id: i.stockId,
      });
      checkStockItem.name = checkStockItem.stock.name;
      checkStockItem.minimum = checkStockItem.stock.minimum;
      checkStockItem.price = checkStockItem.stock.price;
      checkStockItem.unit = checkStockItem.stock.unit;
      checkStockItem.balance = checkStockItem.stock.balance;
      // eslint-disable-next-line prettier/prettier
      checkStockItem.amount = i.amount;

      await this.checkStockItemRepository.save(checkStockItem);
      checkstock.checkStockItems.push(checkStockItem);
      console.log('in lopp check stock');
      stock.name = checkStockItem.name;
      stock.minimum = checkStockItem.minimum;
      stock.price = checkStockItem.price;
      stock.unit = checkStockItem.unit;
      stock.balance = checkStockItem.amount;
      await this.stocksRepository.update(i.stockId, stock);
    }
    // this.stocksRepository.save(stock);
    return this.checkStockRepository.save(checkstock);
  }

  findAll() {
    return this.checkStockRepository.find({
      relations: { user: true, checkStockItems: true },
    });
  }

  findOne(id: number) {
    return this.checkStockRepository.findOneOrFail({
      where: { id },
      relations: { checkStockItems: true, user: true },
    });
  }
  async findAllByBranch(branchId: number) {
    console.log('Branch');

    const branch = JSON.stringify(
      await this.branchsRepository.findOneBy({
        id: branchId,
        stocks: true,
        user: true,
      }),
    );
    console.log(branch);

    const checkstock = await this.checkStockRepository.find({
      where: { branch: JSON.parse(branch) },
      relations: { user: true, checkStockItems: true },
    });

    // console.log(stocks);

    return checkstock;
  }

  async reportMonth(month: string) {
    console.log('REPORT Month FUNCTION');

    console.log('Month ' + month);
    const result = await this.checkStockRepository.query(
      `SELECT 
      DATE_FORMAT(B.date, '%Y-%m') AS Month,
      A.name AS Item,
      SUM(A.balance) AS Balance
  FROM 
      check_stock_item A
  INNER JOIN 
      checkstock B ON A.checkstockId = B.id
  WHERE 
      MONTH(B.date) = MONTH('${month}-01')
  GROUP BY 
      Month, Item;`,
    );
    console.log(result);
    return result;
  }
  async reportUsedMonth(month: string) {
    console.log('REPORT Month FUNCTION');

    console.log('Month Used ' + month);
    const result = await this.checkStockRepository.query(
      `SELECT 
      DATE_FORMAT(B.date, '%Y-%m') AS Month,
      A.name AS Item,
      ROUND(SUM(A.amount) / COUNT(DISTINCT DATE(B.date)) + SUM(C.unitPerItem), 0) AS UsageAmount
  FROM 
      check_stock_item A
  INNER JOIN 
      checkstock B ON A.checkstockId = B.id
  INNER JOIN 
      receipt_stock_item C ON A.stockId = C.stockId
  WHERE 
      MONTH(B.date) = MONTH('${month}-01')
  GROUP BY 
      Month, Item;`,
    );
    console.log(result);
    return result;
  }

  async reportUsedYear(year: string) {
    console.log('REPORT Month FUNCTION');

    console.log('Year Used ' + year);
    const result = await this.checkStockRepository.query(
      `SELECT 
      DATE_FORMAT(B.date, '%Y') AS Year,
      A.name AS Item,
      ROUND(SUM(COALESCE(A.amount, 0)) / COUNT(DISTINCT YEAR(B.date)) + SUM(COALESCE(C.unitPerItem, 0)), 0) AS UsageAmount
  FROM 
      checkstock B
  INNER JOIN 
      check_stock_item A ON B.id = A.checkstockId
  LEFT JOIN 
      receipt_stock_item C ON A.stockId = C.stockId
  WHERE 
      YEAR(B.date) = YEAR('${year}-01-01')
  GROUP BY 
      Year, Item;
  
  `,
    );
    console.log(result);
    return result;
  }

  update(id: number, updateCheckstockDto: UpdateCheckstockDto) {
    return `This action updates a #${id} checkstock`;
  }

  async remove(id: number) {
    const deleteCheckStock = await this.checkStockRepository.findOneOrFail({
      where: { id },
    });
    await this.checkStockRepository.remove(deleteCheckStock);

    return deleteCheckStock;
  }
}
