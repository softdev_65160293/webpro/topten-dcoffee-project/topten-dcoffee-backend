import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckstockDto } from './create-checkstock.dto';

export class UpdateCheckstockDto extends PartialType(CreateCheckstockDto) {}
