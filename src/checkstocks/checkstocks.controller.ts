import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CheckstocksService } from './checkstocks.service';
import { CreateCheckstockDto } from './dto/create-checkstock.dto';
import { UpdateCheckstockDto } from './dto/update-checkstock.dto';
import { AuthGuard } from 'src/auth/auth.guard';

@UseGuards(AuthGuard)
@Controller('checkstocks')
export class CheckstocksController {
  constructor(private readonly checkstocksService: CheckstocksService) {}

  @Post()
  create(@Body() createCheckstockDto: CreateCheckstockDto) {
    return this.checkstocksService.create(createCheckstockDto);
  }

  @Get()
  findAll() {
    return this.checkstocksService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkstocksService.findOne(+id);
  }
  @Get('/reportMonth/Month/:month')
  reportMonth(@Param('month') month: string) {
    return this.checkstocksService.reportMonth(month);
  }
  @Get('/reportUsedMonth/Month/:month')
  reportUsedMonth(@Param('month') month: string) {
    return this.checkstocksService.reportUsedMonth(month);
  }
  @Get('/reportUsedYear/Year/:year')
  reportUsedYear(@Param('year') year: string) {
    return this.checkstocksService.reportUsedYear(year);
  }
  @Get('/getbranch/branch/:branchId')
  findAllByBranch(@Param('branchId') branchId: string) {
    return this.checkstocksService.findAllByBranch(+branchId);
  }
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckstockDto: UpdateCheckstockDto,
  ) {
    return this.checkstocksService.update(+id, updateCheckstockDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkstocksService.remove(+id);
  }
}
