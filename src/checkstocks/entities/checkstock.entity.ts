// import { Stock } from 'src/stocks/entities/stock.entity';
import { Branch } from 'src/branchs/entities/branch.entity';
import { CheckStockItem } from './checkstockItems.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Checkstock {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, (user) => user.checkStock)
  user: User;

  @Column()
  date: Date;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => CheckStockItem,
    (checkStockItem) => checkStockItem.checkstock,
  )
  checkStockItems: CheckStockItem[];

  @ManyToOne(() => Branch, (checkStock) => checkStock.checkStock)
  branch: Branch;
}
