import { Module } from '@nestjs/common';
import { CheckstocksService } from './checkstocks.service';
import { CheckstocksController } from './checkstocks.controller';
import { Checkstock } from './entities/checkstock.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stock } from 'src/stocks/entities/stock.entity';
import { User } from 'src/users/entities/user.entity';
import { CheckStockItem } from './entities/checkstockItems.entity';
import { Branch } from 'src/branchs/entities/branch.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Checkstock, Stock, User, CheckStockItem, Branch]),
  ],
  controllers: [CheckstocksController],
  providers: [CheckstocksService],
})
export class CheckstocksModule {}
