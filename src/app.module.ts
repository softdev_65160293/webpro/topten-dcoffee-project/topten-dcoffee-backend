import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { DataSource } from 'typeorm';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { MembersModule } from './member/members.module';
import { Member } from './member/entities/member.entity';
import { BranchsModule } from './branchs/branchs.module';
import { Branch } from './branchs/entities/branch.entity';
import { StocksModule } from './stocks/stocks.module';
import { CheckstocksModule } from './checkstocks/checkstocks.module';
import { ReceiptStock } from './receiptstocks/entities/receiptstock.entity';
import { Stock } from './stocks/entities/stock.entity';
import { Checkstock } from './checkstocks/entities/checkstock.entity';
import { ReceiptstocksModule } from './receiptstocks/receiptstocks.module';
import { CheckStockItem } from './checkstocks/entities/checkstockItems.entity';
import { ReceiptStockItem } from './receiptstocks/entities/receiptstockItem.entity';
import { CheckIn } from './checkIn/entities/checkIn.entity';
import { CheckInsModule } from './checkIn/checkIns.module';
import { Product } from './products/entities/product.entity';
import { ProductsModule } from './products/products.module';
import { Typebillcost } from './typebillcosts/entities/typebillcost.entity';
import { TypebillcostsModule } from './typebillcosts/typebillcosts.module';
import { Promotion } from './promotions/entities/promotion.entity';
import { PromotionsModule } from './promotions/promotions.module';
import { BillcostsModule } from './billcosts/billcosts.module';
import { Billcost } from './billcosts/entities/billcost.entity';
import { ReceiptsModule } from './receipts/receipts.module';
import { Receipt } from './receipts/entities/receipt.entity';
import { ReceiptItem } from './receipts/entities/receiptItem.entity';
import { SalarysModule } from './salary/salarys.module';
import { Salary } from './salary/entities/salarys.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      //   type: 'sqlite',
      //   database: 'database.sqlite',
      //   synchronize: true,
      //   logging: false,
      //   entities: [
      //     User,
      //     Member,
      //     Branch,
      //     Stock,
      //     Checkstock,
      //     ReceiptStock,
      //     CheckStockItem,
      //     ReceiptStockItem,
      //     CheckIn,
      //     Product,
      //     Typebillcost,
      //     Promotion,
      //     Billcost,
      //     Receipt,
      //     ReceiptItem,
      //     Salary,
      //   ],
      // }),
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'webpro',
      password: 'Pass@1234',
      database: 'webpro',
      entities: [
        User,
        Member,
        Branch,
        Stock,
        Checkstock,
        ReceiptStock,
        CheckStockItem,
        ReceiptStockItem,
        CheckIn,
        Product,
        Typebillcost,
        Promotion,
        Billcost,
        Receipt,
        ReceiptItem,
        Salary,
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    UsersModule,
    AuthModule,
    MembersModule,
    BranchsModule,
    StocksModule,
    CheckstocksModule,
    ReceiptstocksModule,
    CheckInsModule,
    ProductsModule,
    TypebillcostsModule,
    PromotionsModule,
    BillcostsModule,
    ReceiptsModule,
    SalarysModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
